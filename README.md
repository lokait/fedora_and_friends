# Fedora and Friends.
This contains the source SVG and uncompressed desktop wallpapers in PNG format, see Directories for locations.

## Directories.
- Source: contains the source image in *Inkscape SVG*. You can open the image in Inkscape and *Save As* any other format available.
- Wallpapers: contains desktop wallpapers exported in *PNG* format.

## Editing the speech text.
You can add any text in the speech bubble area easily by following these steps:

- Open the source image in Inkscape.
- Select *Layer* > *Layers...*.
- - Now you should see all layers in a menu visible to the right of image.
- - You can click on the *eye* symbol to show/hide the layer and you can click the *lock* symbol to lock/unlock the layer for editing.
- Click on the *lock* symbol of layer titled "Speech \[text\] to unlock it".
- Select the text tool (the symbol is usually **A|** or **A**, the title is "Create and edit text objects...") from tool menu visible to the left of image. \[If hidden make it visible using *View* > *Show/Hide* > *Toolbox*\]
- Click on the text in image.
- - Now you can edit as you would in a simple text editor.
- You can use the menu visible above the image to change basic things like font, size, spacing and alignment. \[If hidden make it visible using *View* > *Show/Hide* > *Tool Control Bar*\].

## Editing everything else.
If you do not know how to do something in Inkscape:

- Search on the Internet.
- You may ask me here but try searching first. I am usually busy with learning, I am not an expert, and I am slowww.

## Credits.
Style: "Uwuntu".

## License.
Copyleft work. Free Art License 1.3. See [LICENSE](/LICENSE) file or [artlibre.org](https://artlibre.org/licence/lal/en).
